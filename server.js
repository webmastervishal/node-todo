require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');

const {mongoose} = require('./db/mongoose');
const {Todo} = require('./models/todo');
const {User} = require('./models/user');
const {ObjectID} = require('mongodb');
const {authenticate} = require('./middlewares/authenticate');
const port = process.env.PORT;

var app = express();

app.use(bodyParser.json());

app.post('/todos', authenticate, (req, res) => {

	var newTodo = new Todo({
		text: req.body.text,
		completed: req.body.completed,
		_creator: req.user._id
	});
	
	newTodo.save().then((todo) => {
		res.send(todo);
	}).catch((e) => {
		res.status(404).send(e);
	});
});

app.get('/todos', authenticate, (req, res) => {
	Todo.find({
		_creator: req.user._id
	}).then((todos) => {
		if(!todos)
			return res.status(404).send('No todos found');

		res.send(JSON.stringify(todos, undefined, 2));
	}).catch((e) => {
		res.status(404).send(e);
	});
});

app.get('/todos/:id', authenticate, (req,res) => {
	var id = req.params.id;

	if(!ObjectID.isValid(id))
		return res.status(404).send('Invalid ID');

	Todo.findOne({
		_id: id,
		_creator: req.user._id
	}).then((todo) => {
		if(!todo)
			return res.status(404).send('No records found');
		res.send(todo);
	}).catch((e) => {
		res.status(404).send(e);
	});
});

app.delete('/todos/:id', authenticate, (req, res) => {
	var id = req.params.id;

	if(!ObjectID.isValid(id))
		return res.status(404).send('Invalid ID');

	Todo.findOneAndRemove({
		_id: id,
		_creator: req.user._id
	}).then((todo) => {
		if(!todo)
			return res.status(400).send('Record not deleted');

		res.send(todo);
	}).catch((e) => {
		res.status(404).send(e);
	});
});


app.patch('/todos/:id', authenticate, (req, res) => {

	var id = req.params.id;
	var body = _.pick(req.body, ['text', 'completed']);

	if(!ObjectID.isValid(id)){
		return res.status(404).send('Invalid ID');
	}

	if(_.isBoolean(body.completed) && body.completed){
		
		body.completedAt = new Date().getTime();
	}
	else{
		body.completed = false;
		body.completedAt = null;
	}

	Todo.findOneAndUpdate({ _id: id, _creator: req.user._id}, {$set: body}, {new:true}).then((todo) => {
		if(!todo){
			return res.status(404).send('Record want updated');
		}
		res.send({todo});

	}).catch((e)  => {
		res.status(400).send(e);
	});

});

//User Models

app.post('/users', (req, res) => {
	var body = _.pick(req.body, ['email','password']);

	var user = new User(body);

	user.save().then(() => {
		return user.generateAuthToken();
	}).then((token) => {
		res.header('x-auth',token).send(user);
	}).catch((e) => {
		res.status(400).send(e);
	});
});


app.get('/users/me', authenticate, (req,res) => {
	res.send(req.user);
});

app.post('/users/login', (req,res) => {
	var body = _.pick(req.body,['email','password']);

	User.findByCredentials(body.email, body.password).then((user) => {
		return user.generateAuthToken().then((token) => {
			res.header('x-auth',token).send(user);
		});
	}).catch((e) => {
		res.status(400).send(e);
	});
});

app.listen(port, () => {
	console.log(`Server is listening at port ${port}...`);
});