const mongoose = require('mongoose');

var Todo = mongoose.model('Todo', { 
	text: {
		type: String,
		minlength: 1,
		trim:true,
		required:true
	},
	completed: Boolean,
	completedAt: Number,
	_creator: {
		type: mongoose.Schema.Types.ObjectId,
		required:true
	}
});

module.exports = {Todo}